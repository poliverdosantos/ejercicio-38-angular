import { Component, OnInit } from '@angular/core';
import { I_Persona } from 'src/app/interfaces/interfas';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  persona: I_Persona ={
    nombre: '',
    correo: '',
  }
  constructor() { }

  ngOnInit(): void {
    
  }

}
